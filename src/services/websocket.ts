import {
  ExpressService,
  FrameworkStartLevel,
  SocketListener,
  WebsocketService,
} from "../types";
import io, { Server, Socket } from "socket.io";
let ws: io.Server;
let subscriptions: SocketListener<any>[] = [];

const webSocketService = new WebsocketService(
  "websocket",
  "Websocket server",
  "Realtime communication interface for 3rd party interfaces",
  "1.0",
  FrameworkStartLevel.INTERNAL_SERVICES,
  undefined,
  true
);

webSocketService.server = () => webSocketService.createPromise(() => ws);

webSocketService.start = async () => {
  let express = await webSocketService.framework
    .getService<ExpressService>("express")
    .server()
    .catch(() => webSocketService.crash("Could not conenct to HTTP Server"));

  if (!express) return;

  ws = new Server(express);

  ws.on("connect", (socket: Socket) => {
    socket.use((packet: any[], next) =>
      onMessage(socket, packet[0], packet[1])
    );

    subscriptions
      .filter((hook) => hook.channel === "onConnect")
      .forEach((hook) => hook.handler(socket));
  });

  webSocketService.resolveAll(ws);
};

webSocketService.stop = () => {
  var sockets = ws.sockets.sockets;
  for (var socketId in sockets) {
    var sock: io.Socket | undefined = sockets.get(socketId);
    sock?.disconnect(true);
  }
  ws.close();
};

webSocketService.subscribe = <T>(listener: SocketListener<T>) => {
  subscriptions.push(listener);
};

webSocketService.unsubscribe = (channel: string) => {
  subscriptions = subscriptions.filter((sub) => sub.channel !== channel);
};

webSocketService.broadcast = (channel: string, message: unknown) => {
  ws.emit(channel, message);
};

function onMessage(socket: Socket, channel: string, ...message: any[]) {
  if (message) {
    subscriptions
      .filter((sub) => sub.channel === channel)
      .forEach((sub) => sub.handler(...message));
  }
}

module.exports = webSocketService;

import express from "express";
import { Server } from "http";
import { ExpressService, FrameworkStartLevel, ServiceConfig } from "..";

export interface ExpressConfig extends ServiceConfig {
  port: number;
}

const configSchema: any = {
  properties: {
    port: {
      type: "number",
      title: "Port",
      default: 3000,
    },
  },
  required: ["port"],
};

let config: ExpressConfig = {
  port: 3000,
  schema: configSchema,
};

const app = express();
const router = express.Router();
app.use("/", router);
let server: Server;

const expressService = new ExpressService(
  "express",
  "Express HTTP Server",
  "Exposes rest endpoints for frontend configuration",
  "1.0",
  router,
  config
);

expressService.server = () => expressService.createPromise(() => server);

expressService.start = async () => {
  server = app.listen(config.port);
  expressService.resolveAll(server);
};

expressService.stop = async () => {
  if (server) server.close();
  expressService.framework.stopService(expressService);
};

module.exports = expressService;

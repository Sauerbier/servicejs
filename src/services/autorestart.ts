import { RecurrenceRule } from "node-schedule";
import { Service, ServiceConfig, ServiceState } from "..";
import { FrameworkEvents, FrameworkStartLevel } from "../types";

export interface AutostartConfig extends ServiceConfig {
  autoRestart: string[];
}

let dynamicSchema: any = {
  properties: {
    autorestart: {
      type: "object",
      title: "Auto Restart",
      description: "Automatically restarts any crashed service.",
      properties: {},
    },
  },
};

let config: AutostartConfig = {
  autoStart: [],
  autoRestart: [],
  schema: dynamicSchema,
  parser: (data: any) => {
    config.autoRestart = Object.keys(data.autorestart)
      .filter((key) => data.autorestart[key])
      .map((key) => key);

    Object.keys(dynamicSchema.properties.autorestart.properties).forEach(
      (key) => {
        dynamicSchema.properties.autorestart.properties[key].default =
          data.autorestart[key];
      }
    );
  },
};

let running = false;

const autostart = new Service(
  "autorestart",
  "AutoRestart",
  "Restarts crashed services",
  "1.0",
  FrameworkStartLevel.KERNEL_SERVICES,
  config
);

autostart.start = async () => {
  running = true;

  autostart.framework
    .getServices()
    .filter(
      (service) => service.startLevel >= FrameworkStartLevel.USER_SERVICES
    )
    .forEach((service) => {
      dynamicSchema.properties.autorestart.properties[service.namespace] = {
        type: "boolean",
        title: service.name,
        default: config.autoRestart?.includes(service.namespace),
      };
    });

  autostart.on(
    FrameworkEvents.SERVICE_CRASH,
    (service: Service, error: string) => {
      autostart.awake(() => {
        if (config.autoRestart.includes(service.namespace)) {
          autostart.framework.startService(service);
        }
      });
    }
  );

  //setTimeout(() => (autostart.state = ServiceState.IDLE), 1000);
};

module.exports = autostart;

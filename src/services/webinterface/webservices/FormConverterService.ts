import { Service, ServiceConfig } from "../../../types";

export interface FormConverterService {
  toForm: (config: Service) => any;
  toConfig: (form: any) => ServiceConfig;
}

const service: FormConverterService = {
  toForm: (service: Service) => {
    if (!service.config?.schema) return undefined;

    let props: any = service.config.schema.properties;

    Object.keys(props).forEach((key) => {
      if (service.config && !props[key].default) {
        props[key].default = service.config[key];
        if (Array.isArray(service.config[key])) {
          props[key].items = { type: "string", default: "" };
        }
      }
    });

    const schema = {
      title: service.name,
      description: service.description,
      type: "object",
      properties: props,
      required: service.config.schema.required,
    };

    return schema;
  },
  toConfig: (form: any) => {
    return form as ServiceConfig;
  },
};

const _ = service;
export default _;

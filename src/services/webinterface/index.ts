import { Request, Response, Router } from "express";
import { Server } from "http";
import { ServiceJs } from "../../";
import {
  ExpressService,
  FrameworkEvents,
  FrameworkStartLevel,
  Service,
  ServiceState,
  WebsocketService,
} from "../../types";
import express from "express";
import path from "path";
import io from "socket.io";
import { registerController } from "./ControllerManager";

export interface WebInterfaceConfig {
  enable: boolean;
}

const config: WebInterfaceConfig = { enable: true };

const webinterfaceService = new Service(
  "webinterface",
  "ServiceJS Webinterface",
  "Provides a UI based configuration system deliverd trough webpages",
  "1.0",
  FrameworkStartLevel.INTERNAL_SERVICES,
  config,
  true
);

webinterfaceService.start = async () => {
  if (!webinterfaceService.config.enable) {
    webinterfaceService.state = ServiceState.IDLE;
    return;
  }

  let framework = webinterfaceService.framework;

  const expressService: ExpressService =
    framework.getService<ExpressService>("express");
  const server: Server | undefined = await expressService
    .server()
    .catch(() => webinterfaceService.crash("Could not talk to http server!"));

  const socketService: WebsocketService =
    framework.getService<WebsocketService>("websocket");
  const socketServer: io.Server | undefined = await socketService
    .server()
    .catch(() => webinterfaceService.crash("Could not talk to socket server!"));

  startWebinterface(framework, expressService.router, server);
  registerWebsocketListeners(socketService, socketServer);

  webinterfaceService.on(
    FrameworkEvents.SERVICE_CRASH,
    (service: Service, error: string) =>
      sendCrashMessage(service, error, socketService)
  );

  webinterfaceService.on(FrameworkEvents.SERVICE_UPATE, (service: Service) =>
    updateState(service, socketService)
  );

  webinterfaceService.stop = async () => stopWebinterface(socketService);
};

function startWebinterface(
  framework: ServiceJs,
  router: Router,
  server?: Server
) {
  router.use(express.static(path.join(__dirname, "../../frontend/build")));
  webinterfaceService.log(
    "loading frontend: " + path.join(__dirname, "../../frontend/build")
  );
  registerController(router, framework);
}

function stopWebinterface(service: WebsocketService) {
  //TODO: disable controllers
  unregisterWebsocketListeners(service);
}

//controllers

function registerWebsocketListeners(
  service: WebsocketService,
  server?: io.Server
) {
  let framework = webinterfaceService.framework;

  service.subscribe({
    channel: "onConnect",
    handler: () => updateServices(service),
  });

  service.subscribe({
    channel: "service-toggle",
    handler: async (data: any) => {
      const target = framework.getService(data.namespace);
      if (target) {
        if (target.state !== ServiceState.STOPPED) {
          await framework.stopService(target);
        } else {
          await framework.startService(target, target.config);
        }
        updateServices(service);
      }
    },
  });

  service.subscribe({
    channel: "service-restart",
    handler: async (data: any) => {
      const target = framework.getService(data.namespace);
      if (target) {
        framework.restartService(target);
      }
    },
  });
}

function unregisterWebsocketListeners(service: WebsocketService) {
  service.unsubscribe("onConnect");
  service.unsubscribe("service-toggle");
  service.unsubscribe("service-restart");
}

function updateServices(ws: WebsocketService) {
  const services = ws.framework.getServices();
  const data = services.map((service) => ({
    namespace: service.namespace,
    name: service.name,
    description: service.description,
    version: service.version,
    state: service.state,
    required: service.startLevel,
    error: service.crashed,
  }));
  ws.broadcast("service-update", data);
}

function updateState(service: Service, ws: WebsocketService) {
  ws.broadcast("service-state", {
    namespace: service.namespace,
    state: service.state,
  });
}

function sendCrashMessage(
  service: Service,
  error: string,
  ws: WebsocketService
) {
  ws.broadcast("service-crash", { namespace: service.namespace, error });
}

module.exports = webinterfaceService;

export * from "./ControllerManager";

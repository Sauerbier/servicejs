import { Request, Response } from "express";
import { ServiceJs } from "../../../servicejs";
import { FrameworkEvents, WebController } from "../../../types";
import formConverterService from "../webservices/FormConverterService";

export class ServiceController extends WebController {
  constructor(framework: ServiceJs) {
    super(framework);
  }
  public getAllServices(req: Request, res: Response) {
    const services = super.framework.getServices();
    const info = services.map((service) => ({
      namespace: service.namespace,
      name: service.name,
      description: service.description,
      version: service.version,
      state: service.state,
      required: service.startLevel,
    }));
    res.send(info);
  }

  public getServiceConfigSchema(req: Request, res: Response) {
    const services = super.framework.getServices();
    const target = services.find(
      (service) => service.namespace === req.params.namespace
    );

    if (target && target.config?.schema) {
      res.send({
        schema: formConverterService.toForm(target),
        uiSchema: target.config.uiSchema,
      });
    } else {
      res.sendStatus(404);
    }
  }

  public saveConfiguration(req: Request, res: Response) {
    const data = req.body;
    const service = super.framework.getService(req.params.namespace);

    if (data && service && service.config) {
      if (service.config.parser) {
        service.config.parser(data);
      } else {
        //default paresr
        //@ts-ignore
        Object.keys(data).forEach((key) => (service.config[key] = data[key]));
      }

      super.framework.saveConfig(service);
      super.framework.emit(FrameworkEvents.SERVICE_CONFIG, service);
      res.sendStatus(200);
      return;
    }

    res.sendStatus(404);
  }
}

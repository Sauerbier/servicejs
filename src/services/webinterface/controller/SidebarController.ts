import { Request, Response } from "express";
import { ServiceJs } from "../../..";
import { WebController } from "../../../types";

let staticRoutes = [
  {
    _component: "CNavItem",
    anchor: "Dashboard",
    to: "/dashboard",
    icon: "cil-speedometer",
  },
];

export class SidebarController extends WebController {
  constructor(framework: ServiceJs) {
    super(framework);
  }
  public getNavigation(req: Request, res: Response) {
    const services = super.framework.getServices();

    const parentRoute: any = {
      _component: "CNavGroup",
      anchor: "Services",
      route: "/to",
      icon: "cil-puzzle",
      items: [],
    };

    parentRoute.items = services
      .filter((service) => service.config && service.config.schema)
      .map((service) => ({
        _component: "CNavItem",
        anchor: service.name,
        to: "/service/" + service.namespace,
        items: undefined,
        icon: undefined,
      }));

    res.send(staticRoutes.concat(parentRoute));
  }
}

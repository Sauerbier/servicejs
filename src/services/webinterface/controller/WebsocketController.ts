import { Request, Response } from "express";
import { ServiceJs } from "../../..";
import { WebController } from "../../../types";

export class WebsocketController extends WebController {
  constructor(framework: ServiceJs) {
    super(framework);
  }
  public getWebsocket(req: Request, res: Response) {
    res.send(process.env.WEBSOCKET_SERVER_URL);
  }
}

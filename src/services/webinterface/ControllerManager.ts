import { Router } from "express";
import { WebsocketController } from "./controller/WebsocketController";
import { ServiceController } from "./controller/ServiceController";
import { SidebarController } from "./controller/SidebarController";
import { ServiceJs } from "../../servicejs";

const bodyParser = require("body-parser");
const jsonParser = bodyParser.json();

export function registerController(router: Router, framework: ServiceJs): void {
  let serviceController = new ServiceController(framework);
  let websocketController = new WebsocketController(framework);
  let navigationControler = new SidebarController(framework);

  //TODO: build a framework to auto register controllers. Maybe make the services too...

  router.get("/api/webinterface/services", (req, res) =>
    serviceController.getAllServices(req, res)
  );
  router.get("/api/webinterface/service/:namespace", (req, res) =>
    serviceController.getServiceConfigSchema(req, res)
  );
  router.post("/api/webinterface/service/:namespace", jsonParser, (req, res) =>
    serviceController.saveConfiguration(req, res)
  );
  router.get("/api/webinterface/websocket", (req, res) =>
    websocketController.getWebsocket(req, res)
  );
  router.get("/api/webinterface/nav", (req, res) =>
    navigationControler.getNavigation(req, res)
  );
}

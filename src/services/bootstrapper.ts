import {
  FrameworkEvents,
  FrameworkStartLevel,
  Service,
  ServiceConfig,
  ServiceState,
} from "../types";

export interface BootsrapperConfig extends ServiceConfig {
  defaultStartLevel: number;
}

const config: BootsrapperConfig = {
  defaultStartLevel: FrameworkStartLevel.HIDDEN_SERVICES,
};

const bootstrapper = new Service(
  "bootstrapper",
  "Bootstrapper",
  "Starts services depending on Startlevel",
  "1.0",
  FrameworkStartLevel.KERNEL_SERVICES,
  config
);

bootstrapper.start = () => {
  bootstrapper.on(FrameworkEvents.FRAMEWORK_START_LEVEL, (level) => {
    bootstrapper.awake(() => {
      let increased = false;
      bootstrapper.framework
        .getServices()
        .filter((service) => service.startLevel <= level)
        .filter(
          (service) =>
            service.state === ServiceState.ERRORED ||
            service.state === ServiceState.STOPPED
        )
        .forEach((service) => {
          if (!increased) {
            bootstrapper.log(`Increasing StartLevel to ${level}...`);
            increased = true;
          }
          let serviceConfig = bootstrapper.framework.loadConfig(service);
          bootstrapper.framework.startService(service, serviceConfig);
        });
    });
  });

  bootstrapper.framework.startlevel = config.defaultStartLevel;
};

module.exports = bootstrapper;

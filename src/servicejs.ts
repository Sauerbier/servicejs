import path from "path";
import fs from "fs";
import {
  FrameworkEvents,
  FrameworkStartLevel,
  Service,
  ServiceConfig,
  ServiceState,
} from "./types";
import EventEmitter from "events";

export class ServiceJs extends EventEmitter {
  private services: Service[] = [];
  private configFolder: string;
  private serviceFolder: string;
  private _startlevel: number;

  constructor(servicePath: string, configPath: string) {
    super();

    this.configFolder = configPath;
    this.serviceFolder = servicePath;
    this._startlevel = 0;

    if (!fs.existsSync(this.configFolder)) {
      fs.mkdirSync(this.configFolder, { recursive: true });
    }

    if (!fs.existsSync(this.serviceFolder)) {
      fs.mkdirSync(this.serviceFolder, { recursive: true });
    }

    let that = this;
    let internalServices = path.join(__dirname, "services");

    fs.readdirSync(internalServices).forEach(function (file) {
      file = path.join(internalServices, file);
      that.loadService(file);
    });

    fs.readdirSync(this.serviceFolder).forEach(function (file) {
      that.loadService(path.join(servicePath, file));
    });
  }

  private loadService(filePath: string) {
    if (
      (filePath.endsWith(".js") ||
        filePath.endsWith(".ts") ||
        fs.lstatSync(filePath).isDirectory()) &&
      !filePath.endsWith(".d.ts")
    ) {
      if (fs.lstatSync(filePath).isDirectory()) {
        let indexFile = fs
          .readdirSync(filePath)
          .find(
            (subFile) =>
              subFile.includes("index.js") || subFile.includes("index.ts")
          );

        filePath = indexFile ? path.join(filePath, indexFile) : filePath;
      }
      const service = require(filePath);
      this.registerService(service);
    }
  }

  public registerService(service: Service) {
    service.framework = this;
    this.services.push(service);
  }

  public loadConfig(service: Service) {
    if (!service.config) return;
    const serviceConfigPath: string = path.join(
      this.configFolder,
      service.namespace + ".json"
    );
    if (!fs.existsSync(serviceConfigPath)) {
      return;
    }

    const confString = fs.readFileSync(serviceConfigPath).toString();
    if (confString) {
      const conf: ServiceConfig = JSON.parse(confString);
      if (conf) {
        conf.schema = service.config.schema;
        conf.parser = service.config.parser;
        conf.uiSchema = service.config.uiSchema;
        service.config = conf;
        return conf;
      }
    }
  }

  public saveConfig(service: Service): void {
    if (!service.config) return;
    const serviceConfigPath: string = path.join(
      this.configFolder,
      service.namespace + ".json"
    );

    fs.writeFileSync(
      serviceConfigPath,
      JSON.stringify(service.config, (key, value) =>
        key === "schema" || key === "parser" ? undefined : value
      )
    );
    this.restartService(service);
  }

  public hasConfig(service: Service): boolean {
    if (!service.config) return false;
    return fs.existsSync(
      path.join(this.configFolder, service.namespace + ".json")
    );
  }

  public getService<T extends Service>(namespace?: string): T {
    return this.services.find(
      (service) => service.namespace === namespace
    ) as T;
  }

  public getServices() {
    return this.services;
  }

  public get startlevel(): number {
    return this._startlevel;
  }
  public set startlevel(value: number) {
    if (value > this._startlevel) {
      while (this._startlevel < value) {
        this._startlevel = this._startlevel + 1;
        this.emit(FrameworkEvents.FRAMEWORK_START_LEVEL, this._startlevel);
      }
    }
  }

  public startServices() {
    console.log("[FRAMEWORK] - LOG: starting kernel services... ");

    this.services.forEach((service) => {
      let config;
      if (!this.hasConfig(service)) this.saveConfig(service);
      else config = this.loadConfig(service);

      if (service.startLevel === FrameworkStartLevel.KERNEL_SERVICES) {
        this.startService(service, config ? config : service.config);
      }
    });
  }

  public async restartService(service: Service) {
    await this.stopService(service);
    this.startService(service, service.config);
  }

  public async startService(service: Service, config: any = service.config) {
    if(service.state === ServiceState.RUNNNING || service.state === ServiceState.IDLE) return;

    console.log(
      `[FRAMEWORK] - LOG: Starting Service: ${service.name} :: v${service.version} - ${service.description} ...`
    );
    try {
      await service._start(config);
    } catch (error) {
      service.crash("" + error);
    }
  }

  public async stopService(service: Service) {
    console.log(
      `[FRAMEWORK] - LOG: Stopping Service: ${service.name} :: v${service.version} - ${service.description} ...`
    );
    await service._stop();
  }
}

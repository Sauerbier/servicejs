import { Router } from "express";
import { Server } from "http";
import io from "socket.io";
import { JSONSchema6 } from "json-schema";
import { ServiceJs } from "..";
import { EventEmitter } from "events";
import scheduler, { Job } from "node-schedule";

export enum ServiceState {
  RUNNNING = 0,
  IDLE = 1,
  STARTING = 2,
  STOPPED = 3,
  ERRORED = 4,
}

export enum FrameworkStartLevel {
  KERNEL_SERVICES = 0,
  INTERNAL_SERVICES = 10,
  HIDDEN_SERVICES = 20,
  USER_SERVICES = 30,
}

export enum FrameworkEvents {
  SERVICE_START = "service_start",
  SERVICE_STOP = "service_stop",
  SERVICE_CRASH = "service_crash",
  SERVICE_UPATE = "service_update",
  SERVICE_CONFIG = "service_config",
  FRAMEWORK_START_LEVEL = "framework_start_level",
}

export enum InternalServices {
  EXPRESS_SERVER = "express",
  WEBINTERFACE = "webinterface",
  WEBSOCKET_SERVER = "websocket",
  BOOTSTRAPPER = "bootstrapper",
}

export interface PromiseManager<T> {
  promise: Promise<T>;
  resolve: (t: T) => void;
  reject: () => void;
}

interface LocalEmitter {
  event: string | symbol;
  listener: (...args: any[]) => void;
}

export abstract class PromiseFactory<T> {
  private promises: PromiseManager<T>[] = [];
  private _framework: ServiceJs = {} as ServiceJs;
  private _timeout: number = 10_000;

  public get timeout(): number {
    return this._timeout;
  }
  public set timeout(value: number) {
    this._timeout = value;
  }

  get framework(): ServiceJs {
    return this._framework;
  }
  set framework(value: ServiceJs) {
    this._framework = value;
  }

  rejectAll(): undefined {
    this.promises.forEach((promise) => promise.reject());
    this.promises = [];
    return undefined;
  }

  resolveAll(t: T) {
    this.promises.forEach((promise) => promise.resolve(t));
    this.promises = [];
  }

  createPromise(value: () => T): Promise<T> {
    if (value()) return new Promise<T>((resolve) => resolve(value()));

    let resolver, rejecter;
    let promise = new Promise<T>((resolve, reject) => {
      resolver = resolve;
      rejecter = reject;

      if (this.shoudTimeout()) {
        setTimeout(() => {
          reject();
        }, this._timeout);
      }
    });

    // @ts-ignore
    this.promises.push({ promise, resolve: resolver, reject: rejecter });
    return promise;
  }

  abstract shoudTimeout(): boolean;
  abstract onTimeout(): void;
}

export interface ConfigSchema {
  properties: JSONSchema6;
  required?: string[];
}

export interface ServiceConfig {
  [id: string]: any;
  schema?: ConfigSchema;
  parse?: (data: unknown) => void;
}
export class Service<T = any> extends PromiseFactory<T> {
  namespace: string;
  name: string;
  description: string;
  version: string;
  startLevel: number;
  config: ServiceConfig;
  crashed?: string;
  daemon: boolean;

  private _shouldTimeout: boolean = true;
  private localEventHandlers: LocalEmitter[] = [];
  private schedules: Job[] = [];
  private _state: ServiceState = ServiceState.STOPPED;

  constructor(
    namespace: string,
    name: string,
    description: string,
    version: string,
    startLevel: number = 30,
    defaultConfig?: ServiceConfig,
    daemon: boolean = false
  ) {
    super();
    this.namespace = namespace;
    this.name = name;
    this.description = description;
    this.version = version;
    this.startLevel = startLevel;
    this.daemon = daemon;
    this.config = defaultConfig ? defaultConfig : {};
  }

  async _start(config: any) {
    this.crashed = undefined;
    this.state = ServiceState.STARTING;
    this.framework.emit(FrameworkEvents.SERVICE_START, this);
    this.state = ServiceState.RUNNNING;
    await this.start();
    if (!this.crashed && !this.daemon) this.state = ServiceState.IDLE;
  }

  async _stop() {
    await this.stop();
    this.schedules.forEach((job) => job.cancel());
    this.resetEventHandlers();
    this.state = ServiceState.STOPPED;
    this.framework.emit(FrameworkEvents.SERVICE_STOP, this);
  }

  shoudTimeout(): boolean {
    return this._shouldTimeout;
  }

  onTimeout() {
    this.crash("Service timed out!");
  }

  crash(error: string): undefined {
    this.crashed = error;
    this.rejectAll();
    this.resetEventHandlers();
    this.state = ServiceState.ERRORED;
    this.framework.emit(FrameworkEvents.SERVICE_CRASH, this, error);
    console.error(`[${this.namespace}] - ERROR: ${error}`);
    return undefined;
  }

  notify(error: string) {
    this.crashed = error;
    this.framework.emit(FrameworkEvents.SERVICE_UPATE, this, this.crashed);
  }

  awake(handler: () => void) {
    this.state = ServiceState.RUNNNING;
    handler();
    this.state = ServiceState.IDLE;
  }

  schedule(
    definition:
      | string
      | number
      | scheduler.RecurrenceRule
      | Date
      | scheduler.RecurrenceSpecDateRange
      | scheduler.RecurrenceSpecObjLit,
    handler: () => void
  ) {
    this.schedules.push(
      scheduler.scheduleJob(definition, () => this.awake(handler))
    );
  }

  clearNotification() {
    this.crashed = "";
    this.framework.emit(FrameworkEvents.SERVICE_UPATE, this, this.crashed);
  }

  private resetEventHandlers() {
    this.localEventHandlers.forEach((emitter: LocalEmitter) => {
      this.framework.off(emitter.event, emitter.listener);
    });
  }

  on(event: string | symbol, listener: (...args: any[]) => void): EventEmitter {
    this.localEventHandlers.push({ event, listener });
    return this.framework.on(event, listener);
  }

  off(
    event: string | symbol,
    listener: (...args: any[]) => void
  ): EventEmitter {
    this.localEventHandlers.splice(
      this.localEventHandlers.indexOf({ event, listener }),
      1
    );
    return this.framework.off(event, listener);
  }

  emit(event: string | symbol, ...args: any[]): boolean {
    return this.framework.emit(event, args);
  }

  start: () => void = async () => {};
  stop: () => void = () => {};

  error(errorMesssage: string) {
    console.error(`[${this.namespace}] - ERROR: ${errorMesssage}`);
  }

  log(logMessage: string) {
    console.log(`[${this.namespace}] - LOG: ${logMessage}`);
  }

  get state() {
    return this._state;
  }

  set state(state: ServiceState) {
    this._state = state;
    this.framework.emit(FrameworkEvents.SERVICE_UPATE, this, state);
  }

  public get shouldTimeout(): boolean {
    return this._shouldTimeout;
  }
  public set shouldTimeout(value: boolean) {
    this._shouldTimeout = value;
  }
}

export class ExpressService extends Service<Server> {
  server!: () => Promise<Server>;
  router: Router;

  constructor(
    namespace: string,
    name: string,
    description: string,
    version: string,
    router: Router,
    config?: ServiceConfig
  ) {
    super(
      namespace,
      name,
      description,
      version,
      FrameworkStartLevel.INTERNAL_SERVICES,
      config,
      true
    );
    this.router = router;
  }
}

export class WebsocketService extends Service<io.Server> {
  server!: () => Promise<io.Server>;
  subscribe!: (listener: SocketListener<any>) => void;
  unsubscribe!: (channel: string) => void;
  broadcast!: (channel: string, message: unknown) => void;
}
export interface SocketMessage {
  channel: string;
  data: any;
}

export interface ServiceInteractMessage extends SocketMessage {
  namespace: string;
  activeState: "ACTIVATE" | "DEACTIVATE";
}

export interface SocketListener<T> {
  channel: string;
  handler: (...t: T[]) => void;
}

export class WebController {
  private _framework: ServiceJs;

  constructor(framework: ServiceJs) {
    this._framework = framework;
  }

  public get framework(): ServiceJs {
    return this._framework;
  }
  public set framework(value: ServiceJs) {
    this._framework = value;
  }
}

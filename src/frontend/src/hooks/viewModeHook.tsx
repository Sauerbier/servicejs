import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { getIconForViewMode, ViewMode } from "../types/ServiceTypes";

const useViewMode = () => {
  const viewMode: number = useSelector((state: any) => state.viewMode);
  const [viewModeIcon, setViewModeIcon] = useState<any>(
    getIconForViewMode(viewMode as ViewMode)
  );

  useEffect(() => {
    setViewModeIcon(getIconForViewMode(viewMode as ViewMode));
  }, [viewMode]);

  return [viewMode, viewModeIcon];
};

export default useViewMode;

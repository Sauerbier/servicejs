import React, { Props } from "react";
import {
  CCol,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CWidgetDropdown,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";

import { CChartLine } from "@coreui/react-chartjs";
import { Service, ServiceState } from "../../../types/ServiceTypes";
import { freeSet } from "@coreui/icons/js/free";
import { Socket } from "socket.io-client";

function getStateStyle(service: Service) {
  switch (service.state) {
    case ServiceState.STARTING:
      return "warning";
    case ServiceState.RUNNNING:
      return "success";
    case ServiceState.IDLE:
      return "secondary";
    case ServiceState.STOPPED:
      return "dark";
    case ServiceState.ERRORED:
      return "danger";
  }
}

export interface CardViewProps {
  service: Service;
  ws: Socket;
}

const RowView = (props: CardViewProps) => {
  return (
    <CCol
      sm="12"
      lg="12"
      key={props.service.namespace}
      style={{ marginBottom: 15 }}
    >
      <CWidgetDropdown
        color={getStateStyle(props.service)}
        //color="primary"
        value={props.service.name}
        title={props.service.description}
        chart={
          <CChartLine
            className="mt-1 mx-1"
            style={{ height: "15px" }}
            data={{} as any}
            options={{}}
            type="line"
          />
        }
        action={
          <CDropdown>
            <CDropdownToggle caret={false} color="transparent">
              <CIcon name="cil-settings" />
            </CDropdownToggle>
            <CDropdownMenu className="pt-0 dark" placement="bottom-end">
              <CDropdownItem
                disabled={
                  props.service.required &&
                  props.service.state !== ServiceState.STOPPED
                }
                onClick={() =>
                  props.ws.emit("service-toggle", {
                    namespace: props.service.namespace,
                  })
                }
              >
                {props.service.state === ServiceState.STOPPED
                  ? "Enable"
                  : "Disable"}
                {props.service.required &&
                  props.service.state !== ServiceState.STOPPED && (
                    <CIcon name="cil-ban" className="pull-right" />
                  )}
              </CDropdownItem>
              <CDropdownItem
                onClick={() =>
                  props.ws.emit("service-restart", {
                    namespace: props.service.namespace,
                  })
                }
              >
                Restart
                <CIcon content={freeSet.cilSync} className="pull-right" />
              </CDropdownItem>
            </CDropdownMenu>
          </CDropdown>
        }
      ></CWidgetDropdown>
    </CCol>
  );
};

export default RowView;

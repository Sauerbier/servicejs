import React, { forwardRef, Props } from "react";
import {
  CCard,
  CCardBody,
  CCardFooter,
  CCardHeader,
  CCol,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CWidgetDropdown,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";

import { CChartLine } from "@coreui/react-chartjs";
import { Service, ServiceState } from "../../../types/ServiceTypes";
import { freeSet } from "@coreui/icons/js/free";
import { Socket } from "socket.io-client";

function getStateStyle(service: Service) {
  switch (service.state) {
    case ServiceState.STARTING:
      return "warning";
    case ServiceState.RUNNNING:
      return "success";
    case ServiceState.IDLE:
      return "secondary";
    case ServiceState.STOPPED:
      return "dark";
    case ServiceState.ERRORED:
      return "danger";
  }
}

export interface CardViewProps {
  service: Service;
  ws: Socket;
}

const CardView = (props: CardViewProps) => {
  return (
    <CCol sm="6" lg="3" key={props.service.namespace} style={{ paddingTop: '15px'}}>
      <CCard color={getStateStyle(props.service)}>
        <CCardHeader className="color-white">
          {props.service.name}
          <CDropdown style={{ float: "right" }}>
            <CDropdownToggle caret={false} color="transparent">
              <CIcon name="cil-settings" />
            </CDropdownToggle>
            <CDropdownMenu className="pt-0 dark" placement="bottom-end">
              <CDropdownItem
                disabled={
                  props.service.required &&
                  props.service.state !== ServiceState.STOPPED
                }
                onClick={() =>
                  props.ws.emit("service-toggle", {
                    namespace: props.service.namespace,
                  })
                }
              >
                {props.service.state === ServiceState.STOPPED
                  ? "Enable"
                  : "Disable"}
                {props.service.required &&
                  props.service.state !== ServiceState.STOPPED && (
                    <CIcon name="cil-ban" className="pull-right" />
                  )}
              </CDropdownItem>
              <CDropdownItem
                onClick={() =>
                  props.ws.emit("service-restart", {
                    namespace: props.service.namespace,
                  })
                }
              >
                Restart
                <CIcon content={freeSet.cilSync} className="pull-right" />
              </CDropdownItem>
            </CDropdownMenu>
          </CDropdown>
        </CCardHeader>
        <CCardBody className="color-white">{props.service.description}</CCardBody>
        {props.service.error && (
          <CCardFooter className="color-white"> {props.service.error} </CCardFooter>
        )}
      </CCard>
    </CCol>
  );
};

export default CardView;


import React, { useEffect, useState } from "react";
import {
  CButton,
  CCol,
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CRow,
  CWidgetDropdown,
} from "@coreui/react";
import { CChartLine } from "@coreui/react-chartjs";
import {
  getIconForViewMode,
  Service,
  ServiceState,
  ViewMode,
} from "../../types/ServiceTypes";
import CIcon from "@coreui/icons-react";
import { cibCodesandbox, freeSet } from "@coreui/icons";
import { io, Socket } from "socket.io-client";
import useWebsocketUrl from "../../hooks/websocketFetcher";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import CardView from "./components/CardView";
import useViewMode from "../../hooks/viewModeHook";
import RowView from "./components/RowView";

let ws: Socket;

const Dashboard = () => {
  const dispatch = useDispatch();
  const filterRequired = useSelector((state: any) => state.filter);
  const [viewMode, viewModeIcon] = useViewMode();
  const [services, setServices] = useState<Service[]>([]);
  const [websocketUrl, setWebsocketUrl] = useWebsocketUrl();
  const router = useHistory();

  useEffect(() => {
    ws = io();
  }, [websocketUrl]);

  useEffect(() => {
    ws.on("service-update", (services: Service[]) => {
      setServices(services);
    });
    ws.on("service-state", (data) => {
      const service = services.find(
        (service) => service.namespace === data.namespace
      );
      if (service) service.state = data.state;
    });
    ws.on("service-crash", (crash: any) => {
      const newServices = services.slice();
      const service = services.find((s) => s.namespace === crash.namespace);
      const i = services.findIndex((s) => s.namespace === crash.namespace);
      if (service) {
        service.state = ServiceState.ERRORED;
        service.error = crash.error;

        newServices[i] = service;
        setServices(newServices);
      }
    });
  }, [websocketUrl, services]);

  return (
    <>
      <CRow>
        <div style={{ marginBottom: 30 }}>
          <CCol sm="12" lg="12">
            <CButton
              color="primary"
              className="pull-right"
              onClick={() => dispatch({ type: "set", filter: !filterRequired })}
            >
              <CIcon
                content={
                  filterRequired ? freeSet.cilFilterX : freeSet.cilFilter
                }
              />
            </CButton>
            <CButton
              color="primary"
              className="pull-right"
              style={{ marginRight: "30px" }}
              onClick={() =>
                dispatch({ type: "set", viewMode: (viewMode + 1) % 2 })
              }
            >
              <CIcon name={viewModeIcon}></CIcon>
            </CButton>
          </CCol>
        </div>
      </CRow>
      <CRow>
        {services
          .sort((a, b) => a.state - b.state)
          .filter((s) => (filterRequired && !s.required) || !filterRequired)
          .map((service) => {
            if (viewMode == ViewMode.CARD) {
              return (
                <CardView ws={ws} service={service} key={service.namespace} />
              );
            } else if (viewMode == ViewMode.ROW) {
              return (
                <RowView ws={ws} service={service} key={service.namespace} />
              );
            }
          })}
      </CRow>
    </>
  );
};

export default Dashboard;

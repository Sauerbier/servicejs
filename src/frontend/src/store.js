import { createStore } from "redux";
import { ViewMode } from "./types/ServiceTypes";

const initialState = {
  sidebarShow: false,
  filter: false,
  viewMode: 0,
};

const changeState = (state = initialState, { type, ...rest }) => {
  switch (type) {
    case "set":
      return { ...state, ...rest };
    default:
      return state;
  }
};

const store = createStore(
  changeState,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
export default store;

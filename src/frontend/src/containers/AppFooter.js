import React from "react";
import { CFooter } from "@coreui/react";

const TheFooter = () => {
  return (
    <CFooter className="dark">
      <div>
        <a
          href="http://sauer-bier.de"
          target="_blank"
          rel="noopener noreferrer"
        >
          ServiceJS
        </a>
        <span className="ml-1">&copy; 2021 MedicyX ltd.</span>
      </div>
      <div className="mfs-auto">
        <span className="mr-1">Powered by</span>
        <a
          href="https://coreui.io/react"
          target="_blank"
          rel="noopener noreferrer"
        >
          CoreUI for React
        </a>
      </div>
    </CFooter>
  );
};

export default React.memo(TheFooter);

import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";

import {
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarToggler,
  CCreateNavItem,
} from "@coreui/react";

import CIcon from "@coreui/icons-react";

import SimpleBar from "simplebar-react";
import "simplebar/dist/simplebar.min.css";
import { NavLink } from "react-router-dom";

// sidebar nav config
import useNavigation from "../hooks/navigationFetcher";

const AppSidebar = () => {
  const dispatch = useDispatch();
  const unfoldable = useSelector((state) => state.sidebarUnfoldable);
  const sidebarShow = useSelector((state) => state.sidebarShow);
  const [navigation, setNavigation] = useNavigation();
  const [navItems, setNavItems] = useState([]);

  useEffect(() => {
    setNavItems(
      navigation.map((nav) => {
        nav.as = NavLink;
        if (nav.icon)
          nav.icon = <CIcon name={nav.icon} customClasses="nav-icon" />;

        if (nav.items) {
          nav.items.forEach((child) => {
            child.as = NavLink;
          });
        }
        return nav;
      })
    );
  }, [navigation]);

  return (
    <CSidebar
      position="fixed"
      selfHiding="md"
      unfoldable={unfoldable}
      show={sidebarShow}
      onHide={() => {
        dispatch({ type: "set", sidebarShow: false });
      }}
    >
      <CSidebarBrand className="d-none d-md-flex" to="/">
        <CIcon
          className="sidebar-brand-full"
          name="logo-negative"
          height={35}
        />
        <CIcon className="sidebar-brand-narrow" name="sygnet" height={35} />
      </CSidebarBrand>
      <CSidebarNav>
        <SimpleBar>
          {navItems.length > 0 && <CCreateNavItem items={navItems} />}
        </SimpleBar>
      </CSidebarNav>
      <CSidebarToggler
        className="d-none d-lg-flex"
        onClick={() =>
          dispatch({ type: "set", sidebarUnfoldable: !unfoldable })
        }
      />
    </CSidebar>
  );
};

export default React.memo(AppSidebar);

import { freeSet } from "@coreui/icons";
import { JSONSchema6 } from "json-schema";

interface Service {
  namespace: string;
  name: string;
  description: string;
  version: string;
  state: ServiceState;
  required: boolean;

  error?: string;
}

interface ServiceSchema {
  schema: JSONSchema6;
  uiSchema: JSONSchema6;
}

enum ServiceState {
  RUNNNING = 0,
  IDLE = 1,
  STARTING = 2,
  STOPPED = 3,
  ERRORED = 4,
}

enum ViewMode {
  CARD = 0,
  ROW = 1,
}

export function getIconForViewMode(mode: ViewMode) {
  switch (mode as ViewMode) {
    case ViewMode.CARD:
      return "cil-view-module";
    case ViewMode.ROW:
      return "cil-view-stream";
  }
}

interface SocketMessage {
  channel: string;
}

export { ServiceState, ViewMode };
export type { Service, ServiceSchema, SocketMessage };

import path from "path";
import { ServiceJs } from "./servicejs";
import { FrameworkStartLevel } from "./types";

export * from "./servicejs";
export * from "./types";
export * from "./services/webinterface";
export * from "./services/webinterface/webservices/FormConverterService";

process.on("uncaughtException", (err: any, origin: any) => {
  console.error(err);
});

process.on("unhandledRejection", (reason, promise) => {
  console.error("Unhandled Rejection at:", promise, "reason:", reason);
});

//TODO: better webinterface api
//TODO: adapt frontend filter to new startlevels
//TODO: frontend list view + button to switch between card view & list view
//TODO: frontend service click open modal / goto service page -> rework this & show service info + config
//TODO: better error / logging handling